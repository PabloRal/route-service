<h1>Route Service</h1>

<h2>Makefile</h2>

```
$ make help
build                          Generates required code and builds debug binary
help                           Displays Makefile help
image                          Builds routeservice:latest docker image
static                         Builds static binary, ready for dockerization

```

<h2>Dockerfile</h2>
You can use ```make image``` to generate docker image of app, then use:<br>

```
$ sudo docker run --publish 8000:8080 routeservice  
```
Now app should be working on ```http://localhost:8000/``` <br>

<h2>Usage</h2>
Application provides to endpoints, the first one is just healthcheck:<br>
```
$ curl http://localhost:8000/status
{
    "uptime":43835997599
}
```
As you can see above, when you make GET request on this one you will get json
with server uptime in milliseconds.

Endpoint ``/routes`` is handling get requests and returns routes(sorted ascending by duration and distance) beteween source 
and destinations given as query parameters(latitude and longitude comma separeted). Example:
```
$ curl http://localhost:8000/routes?src=13.388860,52.517037&dst=13.397634,52.529407&dst=13.428555,52.523219
{
    "source": "13.388860,52.517037",
    "routes": [
        {
            "destination": "13.397634,52.529407",
            "duration": "420.8",
            "distance": "1998.6"
        },
        {
            "destination": "13.428555,52.523219",
            "duration": "649.1",
            "distance": "3794.9"
        }
    ]
}
```
Duration is in second and distances is in meters.

Data provided by http://project-osrm.org/