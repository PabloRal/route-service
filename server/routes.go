package server

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

func (s *server) routes(r *mux.Router) {
	r.HandleFunc("/status", s.statusHandler()).Methods("GET")
	r.HandleFunc("/routes", s.routesHandler()).Methods("GET")
}

type statusResponse struct {
	Uptime time.Duration `json:"uptime"`
}

func (s *server) statusHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK
		response := statusResponse{
			Uptime: time.Since(s.timer),
		}

		responseJson, err := json.Marshal(response)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(responseJson)
	}
}

func (s *server) routesHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK

		urlParams, _ := url.ParseQuery(r.URL.RawQuery)
		if !validateParams(urlParams) {
			s.logger.Println("Wrong params")
			http.Error(w, "Wrong params", http.StatusBadRequest)
			return
		}

		routes, err := s.osrmClient.GetRoutesFor(urlParams["dst"], urlParams["src"][0])
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		responseJson, err := json.Marshal(routes)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(responseJson)
	}
}

func validateParams(urlParams map[string][]string) bool {
	if len(urlParams["src"]) != 1 {
		return false
	}

	if !validateParam(urlParams["src"][0]) {
		return false
	}

	for _, dst := range urlParams["dst"] {
		if !validateParam(dst) {
			return false
		}
	}

	return true
}

func validateParam(param string) bool {
	coordinates := strings.Split(param, ",")
	if len(coordinates) != 2 {
		return false
	}
	if !validateCoordinate(coordinates[0]) {
		return false
	}
	if !validateCoordinate(coordinates[1]) {
		return false
	}

	return true
}

func validateCoordinate(coordinate string) bool {
	if _, err := strconv.ParseFloat(coordinate, 64); err != nil {
		return false
	}
	return true
}
