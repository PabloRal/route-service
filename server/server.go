package server

import (
	"context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/xid"
	"gitlab.com/PabloRal/route-service/osrmservice"
	"log"
	"net/http"
	"os"
	"time"
)

type server struct {
	logger     *log.Logger
	timer      time.Time
	osrmClient osrmservice.Client
}

type key int

const (
	requestIDKey key = 0
)

func newID() string {
	return xid.New().String()
}

func logging(logger *log.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				logger.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
			}()
			next.ServeHTTP(w, r)
		})
	}
}

func tracing(nextRequestID func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = nextRequestID()
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestID)
			w.Header().Set("X-Request-Id", requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func New() http.Handler {
	logger := log.New(os.Stdout, "http: ", log.LstdFlags)

	s := server{
		logger:     logger,
		timer:      time.Now(),
		osrmClient: osrmservice.New(),
	}
	router := mux.NewRouter()
	s.routes(router)

	return tracing(newID)(handlers.CORS(
		handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}))(logging(logger)(router)))
}
