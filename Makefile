.PHONY: build
build: ## Generates required code and builds debug binary
	go generate
	go build
	go test ./...

.PHONY: static
static: ## Builds static binary, ready for dockerization
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s"

.PHONY: image
image: ## Builds routeservice:latest docker image
	docker build -t routeservice .

.PHONY: help
help: ## Displays Makefile help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
