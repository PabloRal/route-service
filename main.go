package main

import (
	"gitlab.com/PabloRal/route-service/server"
	"log"
	"net/http"
)


func main() {
	handler := server.New()
	httpServer := &http.Server{
		Addr: ":8080",
		Handler: handler,
	}

	log.Fatal(httpServer.ListenAndServe())
}