package osrmservice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
)

type OsrmResponse struct {
	Routes []OsrmRoute `json:"routes"`
	Code   string      `json:"code"`
}

type OsrmRoute struct {
	Duration float64 `json:"duration"`
	Distance float64 `json:"distance"`
}

type Route struct {
	Destination string `json:"destination"`
	Duration    string `json:"duration"`
	Distance    string `json:"distance"`
}

type Routes struct {
	Source string  `json:"source"`
	Routes []Route `json:"routes"`
}

const osrmUri string = "http://router.project-osrm.org/route/v1/driving/%s;%s?overview=false"

type Client struct {
	httpClient *http.Client
}

func (c *Client) GetRoutesFor(destinations []string, source string) (Routes, error) {
	var routes []Route
	for _, destination := range destinations {
		resp, err := c.getRouteFor(source, destination)
		if err != nil || resp.Code != "Ok" {
			return Routes{}, err
		}

		route := Route{
			Destination: destination,
			Distance:    floatToString(resp.Routes[0].Distance),
			Duration:    floatToString(resp.Routes[0].Duration),
		}
		routes = append(routes, route)
	}

	sortRoutes(routes)

	result := Routes{
		Source: source,
		Routes: routes,
	}

	return result, nil
}

func (c *Client) getRouteFor(source string, destination string) (OsrmResponse, error) {
	request := fmt.Sprintf(osrmUri, source, destination)
	response, err := c.httpClient.Get(request)
	if err != nil {
		return OsrmResponse{}, err
	}

	var resp OsrmResponse
	jsonResp, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return OsrmResponse{}, err
	}

	err = json.Unmarshal(jsonResp, &resp)
	if err != nil {
		return OsrmResponse{}, err
	}

	return resp, nil
}

func sortRoutes(routes []Route) {
	sort.Slice(routes[:], func(i, j int) bool {
		return routes[i].Duration < routes[j].Duration ||
			(routes[i].Duration == routes[j].Duration && routes[i].Distance < routes[j].Distance)
	})
}

func floatToString(number float64) string {
	return strconv.FormatFloat(number, 'f', 1, 64)
}

func New() Client {
	return Client{
		httpClient: &http.Client{},
	}
}
