package osrmservice

import "testing"

func TestSorting(t *testing.T) {
	var routes []Route
	routes = append(routes, Route{Destination: "0,0", Distance: "110.00", Duration: "100.00"})
	routes = append(routes, Route{Destination: "1,0", Distance: "100.00", Duration: "100.00"})
	routes = append(routes, Route{Destination: "2,0", Distance: "130.00", Duration: "140.00"})

	sortRoutes(routes)

	if routes[0].Destination != "1,0" {
		t.Errorf("First element is %s but should be 1,0", routes[0].Destination)
	}
	if routes[1].Destination != "0,0" {
		t.Errorf("Second element is %s but should be 0,0", routes[1].Destination)
	}
	if routes[2].Destination != "2,0" {
		t.Errorf("Third element is %s but should be 2,0", routes[2].Destination)
	}
}
