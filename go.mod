module gitlab.com/PabloRal/route-service

go 1.14

require (
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/rs/xid v1.2.1
)
